package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"
)

type TimeWarriorEntry struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

type Workday struct {
	Duration time.Duration
	Earned   decimal.Decimal
}

var debug = false
var verbose = false
var version string

const TimeWarriorReportLayout = "20060102T150405Z"
const KeyLayout = "2006-01-02"
const DebugPrintLayour = "Mon 02 15:04:05 MST"

func main() {
	env := os.Getenv("HOURLY_WAGE")
	if len(env) == 0 {
		checkError(errors.New("env variable HOURLY_WAGE not set"))
	}
	hourly, err := decimal.NewFromString(env)
	if err != nil {
		checkError(fmt.Errorf("could not parse hourly wage env variable %q: %s", env, err.Error()))
	}
	if hourly.IsNegative() {
		checkError(fmt.Errorf("env variable HOURLY_WAGE cannot be negative"))
	}
	if verbose {
		fmt.Printf("Hourly wage is %s SEK\n", hourly.String())
	}
	r := parseReport()
	days := make(map[string]Workday)
	for _, e := range r {
		start, err := time.Parse(TimeWarriorReportLayout, e.Start)
		checkError(err)
		end, err := time.Parse(TimeWarriorReportLayout, e.End)
		checkError(err)
		i := end.Sub(start).Round(time.Minute)
		if debug {
			fmt.Printf("%8s from %s to %s\n", i.String(), fmtDate(start), fmtDate(end))
		}
		date := start.Format(KeyLayout)
		work, ok := days[date]
		if !ok {
			work = Workday{}
		}
		work.Duration += i
		duration := decimal.NewFromFloat(work.Duration.Hours())
		work.Earned = hourly.Mul(duration).RoundBank(0)
		days[date] = work
	}
	printWorkdays(days)
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}

func parseReport() []TimeWarriorEntry {
	var r []TimeWarriorEntry
	scanner := bufio.NewScanner(os.Stdin)
	var b strings.Builder
	for scanner.Scan() {
		_, err := fmt.Fprintf(&b, scanner.Text())
		checkError(err)
	}
	if err := scanner.Err(); err != nil {
		checkError(err)
	}
	text := b.String()
	debugRe := buildRe("debug")
	verboseRe := buildRe("verbose")
	if debugRe.MatchString(text) {
		debug = true
	}
	if verboseRe.MatchString(text) {
		verbose = true
	}

	VersionRe := regexp.MustCompile("temp\\.version:\\s+([[:digit:]\\.]+)")
	submatch := VersionRe.FindStringSubmatch(text)
	if submatch != nil {
		version = submatch[1]
		if verbose {
			fmt.Printf("version %s\n", version)
		}
	}

	jsonStart := strings.IndexByte(text, '[')
	jsonText := text[jsonStart:]

	err := json.Unmarshal([]byte(jsonText), &r)
	checkError(err)
	return r
}

func buildRe(setting string) *regexp.Regexp {
	trueValues := "on|1|yes|y|true"
	return regexp.MustCompile(fmt.Sprintf("%s:\\s+(%s)", setting, trueValues))
}

func fmtDuration(d time.Duration) string {
	d = d.Round(time.Minute)
	h := d / time.Hour
	d -= h * time.Hour
	m := d / time.Minute
	return fmt.Sprintf("%02d:%02d", h, m)
}

func fmtDate(t time.Time) string {
	return t.Local().Format(DebugPrintLayour)
}

func printWorkdays(days map[string]Workday) {
	var dates []string
	for date, worktime := range days {
		if worktime.Duration > 0 {
			dates = append(dates, date)
		}
	}
	sort.Strings(dates)
	for _, date := range dates {
		workday := days[date]
		fmt.Printf("%s %6s %8s SEK\n", date, fmtDuration(workday.Duration), workday.Earned.StringFixedBank(2))
	}
}
